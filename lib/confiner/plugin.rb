# frozen_string_literal: true

module Confiner
  # Plugin for Confiner
  class Plugin
    prepend Logger

    attr_reader :examples

    DEFAULT_PLUGIN_ARGS = {
      debug: false,  # Used for debugging logging
      dry_run: false # Used for when external requests are inhibited
    }.freeze

    class << self
      # Define arguments that the plugin will accept
      # @param [Array<Symbol, Hash>] args the arguments that this plugin accepts
      # @option args [Symbol] :argument the argument to pass with no default
      # @option args [Hash] :argument the argument to pass with a default value
      # @note the arguments should be well-formed in the .yml rule file
      def arguments(*args)
        @arguments ||= args

        args.each do |arg|
          if arg.is_a? Hash
            arg.each do |a, default_value|
              attr_writer a

              default = default_value
              default = ENV[default_value[1..]] if default_value[0] == '$'

              define_method(a) do
                instance_variable_get("@#{a}") || instance_variable_set("@#{a}", default)
              end
            end
          else
            attr_accessor arg
          end
        end
      end
    end

    def initialize(options, **args)
      @options = DEFAULT_PLUGIN_ARGS.merge(options)
      @options = Struct.new(*@options.keys).new(*@options.values)

      args.each do |k, v|
        v = ENV[v[1..]] if v[0] == '$' # get environment variable

        self.public_send(:"#{k}=", v)
      end
    end

    # Run the plugin
    def run(action, &block)
      yield self
    end
  end
end
