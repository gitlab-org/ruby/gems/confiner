# GitLab

## Arguments

| argument                | default                     | description                                                                                                                                                                                                                                                                                  |
|-------------------------|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| private_token           |                             | Personal access token used to authenticate with the GitLab API. (Token must have access to the plugin's `target_project` arg)                                                                                                                                                                |
| endpoint                | `https://gitlab.com/api/v4` | GitLab API Endpoint (e.g. https://gitlab.com/api/v4)                                                                                                                                                                                                                                         |
| [threshold](#threshold) | `3`                         | Failure / Success threshold that will trigger a quarantine/dequarantine                                                                                                                                                                                                                      |
| target_project          |                             | Failure issues will be searched in this project, as well as the quarantine/dequarantine MR                                                                                                                                                                                                   |
| project_id              |                             | The ID of the Project where pipelines are fetched                                                                                                                                                                                                                                            |
| failure_issue_labels    |                             | Labels to search for when searching for the failure issue                                                                                                                                                                                                                                    |
| failure_issue_prefix    |                             | Prefix that an issue must have in GitLab Issues when searching for the failure issue                                                                                                                                                                                                         |
| pwd                     | .                           | Path of the working directory for the examples                                                                                                                                                                                                                                               |
| timeout                 | 10                          | Timeout that the Gitlab CLI will pass to HTTParty                                                                                                                                                                                                                                            |
| ref                     | `main`                      | The ref used when querying pipelines, what ref for the MR to request merge into and files to search                                                                                                                                                                                          |
| environment             | { name: nil, pattern: nil } | Information about the environment Confiner is targeting. Specify a name, where the name is nice name of the environment. A pattern where pattern matches the value inside the [`only`](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/execution_context_selection.html) key |

### Threshold

_P = threshold * 2_

Given _threshold = 3_:

In the last _P [6]_ pipelines, if there are _threshold_ passes or failures, a quarantine / dequarantine will be triggered.

## Actions

### `quarantine`

Given the arguments and the threshold, if the amount of test failures reach the threshold, quarantine the test by
adding the metadata `quarantine: { issue: '<failure_issue>', type: :investigating }` to the spec.  Then automatically
open a Merge Request title prefixed with: `[QUARANTINE]` to quarantine the test.

The MR can then be reviewed.

### `dequarantine`

Not yet implemented.

## Examples

```yaml
- name: Quarantine a test that fails consistently on a self-managed GitLab instance
  plugin:
    name: gitlab
    args:
      endpoint: https://my-local-gitlab/api/v4
      private_token: super-secret-token
      project_id: my-group/my-project
      target_project: my-group/my-project
  actions:
    - quarantine

###

- name: Quarantine tests in production GitLab that fails 8 times and an issue search that takes a while due to a lot of issues
  plugin:
    name: gitlab
    args:
      private_token: super-secret-token
      project_id: gitlab-org/gitlab-qa-mirror
      target_project: gitlab-org/gitlab
      pwd: qa
      timeout: 60
  actions:
    - quarantine

- name: Dequarantine tests that pass consistently
  plugin:
    name: gitlab
    args:
      private_token: $API_TOKEN
      project_id: gitlab-org/gitlab-qa-mirror
      target_project: gitlab-org/gitlab
      pwd: qa
      failure_issue_labels: QA,Quality # comma-separated labels
      failure_issue_prefix: "Failure in "
  actions:
    - dequarantine
```

## Potential issues

Confiner matches on Ruby string-literals.  For tests that are named dynamically, issues may arise.

```ruby
# example
it "is #{'dynamic'}"
```
