# Confiner CLI

# .confiner directory

Store rules within a `.confiner` directory for Confiner to read. 

# Running rules

## Run a specific rule file

`$ bin/confiner -r my_rule.yml`

## Run all rule files within a directory

`$ bin/confiner -r my_rules`

