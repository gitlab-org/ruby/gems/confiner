# Plugins

For a list of plugins, see [plugins](plugins/index.md).

## Develop a plugin

- Plugin should extend `Confiner::Plugin`
- Add arguments plugin supports

### Arguments

Arguments are what the plugin takes in as parameters.

Specify arguments using `arguments`.

```ruby
class MyPlugin < Confiner::Plugin
  arguments :a,
            :b,
            :c
  
  def some_action
    puts a
    puts b
    puts c
  end
end
```

Arguments will be available to the instance like an `attr_reader`.

Arguments are passed in the rule Yaml files like so:

```yaml
  - name: Show arguments
    plugin: 
      name: my_plugin
      args:
        a: foo
        b: bar
        c: baz
```

#### Defaults

Arguments can be specified with a default value. Specify all defaulted arguments using `:key => value` notations after non-defaulted arguments. 

```ruby
class MyPlugin < Confiner::Plugin
  arguments :a,
            :b,
            :c => 'default'
end
```

Defaults can be overridden in the YAML by specifying the defaulted argument [in the `args` hash](rules.md#args--hash).

### Actions

Any public methods defined in the plugin is considered an "action".

```ruby
class MyPlugin < Confiner::Plugin
  def action1
  end
  
  def action2
  end
end
```

This plugin has two actions. `action1` and `action2` and can be called in the YAML like so:

```yaml
- name: Call actions
  plugin:
    name: my_plugin
  actions:
    - action1
    - action2
```
