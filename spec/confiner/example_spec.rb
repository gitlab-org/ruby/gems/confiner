# frozen_string_literal: true

require 'spec_helper'

module Confiner
  RSpec.describe Example do
    let(:example_data) do
      {
        status: "success",
        name: "Manage with IP rate limits Users API GET /users",
        classname: "qa.specs.features.api.1_manage.rate_limits_spec",
        file: "./qa/specs/features/api/1_manage/rate_limits_spec.rb",
        execution_time: 918.79123,
        system_output: nil,
        stack_trace: nil,
        recent_failures: nil,

        occurrence: 1
      }
    end

    subject(:example) { described_class.new(**example_data) }

    describe 'accessors' do
      it { is_expected.to respond_to(:status) }
      it { is_expected.to respond_to(:status=) }
      it { is_expected.to respond_to(:name) }
      it { is_expected.to respond_to(:name=) }
      it { is_expected.to respond_to(:classname) }
      it { is_expected.to respond_to(:classname=) }
      it { is_expected.to respond_to(:file) }
      it { is_expected.to respond_to(:file=) }
    end

    describe '#new' do
      context 'with block' do
        subject(:example) do
          described_class.new(**example_data) do |example|
            example.file = 'overwritten'
          end
        end

        it 'sets or overwrites the accessors' do
          expect(example.file).to eq('overwritten')
        end
      end

      context 'without block' do
        subject(:example) do
          described_class.new(**example_data)
        end

        it 'has a file that is "./qa/specs/features/api/1_manage/rate_limits_spec.rb"' do
          expect(example.file).to eq('./qa/specs/features/api/1_manage/rate_limits_spec.rb')
        end

        it 'has a classname that is "qa.specs.features.api.1_manage.rate_limits_spec"' do
          expect(example.classname).to eq('qa.specs.features.api.1_manage.rate_limits_spec')
        end
      end
    end

    describe '#passed?' do
      before do
        allow(example).to receive(:status).and_return('success')
      end

      it 'returns true when the status is "success"' do
        expect(example).to be_passed
      end
    end

    describe '#failed?' do
      before do
        allow(example).to receive(:status).and_return('failed')
      end

      it 'returns true when the status is "failed"' do
        expect(example).to be_failed
      end
    end

    describe '#skipped?' do
      before do
        allow(example).to receive(:status).and_return('skipped')
      end

      it 'returns true when the status is "skipped"' do
        expect(example).to be_skipped
      end
    end
  end
end
