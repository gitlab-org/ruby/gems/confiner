# frozen_string_literal: true

require 'spec_helper'

module Confiner
  RSpec.describe Plugin do
    subject(:plugin) do
      Class.new(described_class).new({ debug: true })
    end

    it { is_expected.to respond_to(:examples) }

    context 'logging' do
      it { is_expected.to respond_to(:log) }
    end

    describe 'arguments' do
      subject(:plugin_with_args) do
        Class.new(described_class) do
          arguments :a, :b, :c
        end.new({ debug: true })
      end

      %i[a b c].each do |arg|
        it { is_expected.to respond_to(arg) }
        it { is_expected.to respond_to("#{arg}=") }
      end

      context 'when argument is $ENVIRONMENT_VARIABLE' do
        subject(:plugin) do
          Class.new(described_class) do
            arguments :a,
                      :b => '$TEST'
          end.new({ debug: true })
        end

        before do
          ENV['TEST'] = 'test'
        end

        after do
          ENV['TEST'] = nil
        end

        it 'returns the environment variable value' do
          expect(plugin.b).to eq(ENV['TEST'])
        end
      end
    end

    describe 'default arguments' do
      subject(:plugin_with_args) do
        Class.new(described_class) do
          arguments :a,
                    :b => 'test'
        end.new({ debug: true })
      end

      it 'has appropriate values', :aggregate_failures do
        expect(plugin_with_args.a).to be_nil
        expect(plugin_with_args.b).to eq('test')
      end
    end

    describe 'actions' do
      subject(:plugin) do
        Class.new(described_class) do
          arguments :a,
                    :b => 'test'

          def action
            b
          end
        end.new({ debug: true })
      end

      it 'has access to arguments' do
        expect(plugin.action).to eq(plugin.b)
      end
    end

    describe 'plugin options' do
      let(:plugin_with_dry_run) do
        Class.new(described_class) do
          def dry_run
            @options.dry_run
          end

          def debug
            @options.debug
          end
        end.new({ dry_run: true })
      end

      let(:plugin_with_debug) do
        Class.new(described_class) do
          def dry_run
            @options.dry_run
          end

          def debug
            @options.debug
          end
        end.new({ debug: true })
      end

      describe 'dry_run' do
        it 'defaults to false' do
          expect(plugin_with_debug.dry_run).to eq(false)
        end

        context 'when set' do
          it 'registers' do
            expect(plugin_with_debug.dry_run).to eq(false)
          end
        end
      end

      describe 'debug' do
        it 'defaults to false' do
          expect(plugin_with_dry_run.debug).to eq(false)
        end

        context 'when set' do
          it 'registers' do
            expect(plugin_with_debug.debug).to eq(true)
          end
        end
      end
    end
  end
end
