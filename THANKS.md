Thank you to the following for being supportive of Free software and assets for the world, to better the world. :heart:

- [NARKOZ](https://github.com/NARKOZ/gitlab) for providing the delightful Gitlab API [Rubygem](https://rubygems.org/gems/gitlab)
- [RSpec and the RSpec contributors](https://rspec.info/)
- Xavier Noria and Contributors for [Zeitwerk](https://github.com/fxn/zeitwerk)
- `gstudioimagen` from [Vecteezy](https://www.vecteezy.com/free-vector/radioactive) for the biohazard icon used with the GitLab Tanuki
